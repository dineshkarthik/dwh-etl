"""create_district_dimension_table

Revision ID: 66138e880054
Revises: 6edadfbd8191
Create Date: 2019-04-30 16:32:54.007240

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '66138e880054'
down_revision = '6edadfbd8191'
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE district 
          ( 
            id   BIGSERIAL PRIMARY KEY, 
            NAME VARCHAR(100) NOT NULL 
          );
    """
    )


def downgrade():
    op.execute(
        """
        DROP TABLE district
    """
    )