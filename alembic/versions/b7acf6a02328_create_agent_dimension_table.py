"""create_agent_dimension_table

Revision ID: b7acf6a02328
Revises: 66138e880054
Create Date: 2019-04-30 16:33:53.648122

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b7acf6a02328'
down_revision = '66138e880054'
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE agent 
          ( 
            id            BIGSERIAL PRIMARY KEY, 
            first_name    VARCHAR(100), 
            last_name     VARCHAR(100), 
            agency_name   VARCHAR(100), 
            phone_number  VARCHAR(20), 
            email_address VARCHAR(50) 
          ); 
    """
    )


def downgrade():
    op.execute(
        """
        DROP TABLE agent
    """
    )