# -*- encoding: ascii -*-
"""
DWH namespace
~~~~~~~~~~~~
Spark Datawarehouse internal libraries and apps.
"""

# WARNING: do not put any more code here.
import pkgutil as _pkgutil

__path__ = _pkgutil.extend_path(__path__, __name__)
