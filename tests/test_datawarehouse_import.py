"""Unittest module for etl-import script."""
import os
import json
import mock

import unittest
import pandas as pd

from sqlalchemy import text

from dwh.etl.datawarehouse_import import (
    get_file_names,
    create_delete_query,
    get_insert_from_temp_table_query,
    copy_to_table,
)


class DataWareHouseImportTestCase(unittest.TestCase):
    """Test DWH import script."""

    @mock.patch("dwh.etl.datawarehouse_import.logger")
    def test_get_file_names(self, mock_logger):
        """Test get_file_names."""
        result = get_file_names("/home/test/etl/data")
        mock_logger.warning.assert_called_with(
            "%s directory does not exists", "/home/test/etl/data"
        )

    def test_create_delete_query(self):
        expected_result = (
            "delete from flat using temp_flat where flat.id = temp_flat.id"
        )
        result = create_delete_query(table_name="flat", update_keys=["id"])
        self.assertEqual(result, expected_result)

    def get_insert_from_temp_table_query(self):
        expected_result_1 = "insert into flat (id,size,rent) select id,size,rent from temp_flat"
        result_1 = get_insert_from_temp_table_query(
            table_name="flat", columns=["id", "size", "rent"]
        )
        self.assertEqual(result_1, expected_result_1)

        expected_result_2 = "insert into city (name) (select name from temp_city left_join using(name) when city is null"
        result_2 = get_insert_from_temp_table_query(
            table_name="city", columns=["name"]
        )
        self.assertEqual(result_1, expected_result_1)

    @mock.patch("dwh.etl.datawarehouse_import.db")
    def test_copy_to_table(self, mock_db):
        copy_to_table("city", "name", "/home/data/test_city.csv")
        mock_db.execute.assert_called_with(
            mock.ANY, csv="/home/data/test_city.csv"
        )
