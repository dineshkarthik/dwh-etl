# Installation Instructions #

## Actual Application ##

Development and deployment is done inside an isolated virtualenv container.
Assuming you have both virtualenv and
[virtualenvwrapper](http://www.doughellmann.com/docs/virtualenvwrapper/)
(installable with pip), create a container and install the application into it:

    mkvirtualenv --python python3.6 dwh-etl
    workon dwh-etl
    pip install --upgrade pip
    cd "$VIRTUAL_ENV"
    git clone https://dineshkarthik@bitbucket.org/dineshkarthik/dwh-etl.git src/dwh-etl
    cd src/dwh-etl
    pip install -r dev-requirements.txt
    echo 'cd "$VIRTUAL_ENV"/src/dwh-etl' >> "$VIRTUAL_ENV"/bin/postactivate

You can now work inside virtual container 'dwh-etl' by executing

    workon dwh-etl
