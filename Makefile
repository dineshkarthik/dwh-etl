
TEST_ARTIFACTS ?= /tmp/coverage

.PHONY: deps requirements install test

requirements:
	pip install --upgrade pip setuptools
	pip install -r requirements.txt

install: requirements
	pip install -e .

deps: install
	pip install -r dev-requirements.txt

test: 
	py.test --cov dwh --doctest-modules \
		--cov-report term-missing \
		--cov-report html:${TEST_ARTIFACTS} \
		--junit-xml=${TEST_ARTIFACTS}/datawarehouse-import.xml \
		tests/ dwh/