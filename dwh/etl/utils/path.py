# -*- encoding: ascii  -*-
"""
Utility function realted to os/file paths.
"""
from pathlib import Path

def get_project_root() -> Path:
    """Returns project root folder."""
    return Path(__file__).parent.parent.parent.parent
