"""create_flat_fact_table

Revision ID: d9ad8f25b3b6
Revises: b7acf6a02328
Create Date: 2019-04-30 16:34:56.558674

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd9ad8f25b3b6'
down_revision = 'b7acf6a02328'
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE flat 
          ( 
            id              BIGINT PRIMARY KEY, 
            size            DOUBLE PRECISION, 
            bed_rooms       INTEGER, 
            bath_rooms      INTEGER, 
            total_rooms     INTEGER, 
            base_rent       DOUBLE PRECISION, 
            total_rent      DOUBLE PRECISION, 
            street          VARCHAR(100), 
            district_id     INTEGER REFERENCES district(id), 
            city_id         INTEGER REFERENCES city(id), 
            agent_id        INTEGER REFERENCES agent(id), 
            created_at      TIMESTAMP, 
            last_updated_at TIMESTAMP, 
            is_active       BOOLEAN, 
            has_pictures    BOOLEAN 
          );  
    """
    )


def downgrade():
    op.execute(
        """
        DROP TABLE flat
    """
    )