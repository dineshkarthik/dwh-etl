"""create_city_dimension_table

Revision ID: 6edadfbd8191
Revises: 
Create Date: 2019-04-30 15:27:23.180822

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6edadfbd8191"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE city 
          ( 
            id   BIGSERIAL PRIMARY KEY, 
            NAME VARCHAR(100) NOT NULL 
          );
    """
    )


def downgrade():
    op.execute(
        """
        DROP TABLE city
    """
    )
