"""Import module."""
import os
import json
import yaml
import shutil
import logging
import datetime as dt

import pandas as pd
from sqlalchemy import create_engine, text


from dwh.etl.utils.path import get_project_root


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


ROOT_DIR = get_project_root()

f = open(os.path.join(ROOT_DIR, "config.yaml"))
config = yaml.safe_load(f)
f.close()

intial_dir = config["intial_dir"]
processed_dir = config["processed_dir"]
import_dir = config["import_dir"]

db = create_engine(config["sqlalchemy.url"])

schema = dict(
    agent="""id            BIGSERIAL, 
            first_name    VARCHAR(100), 
            last_name     VARCHAR(100), 
            agency_name   VARCHAR(100), 
            phone_number  VARCHAR(20), 
            email_address VARCHAR(50)""",
    flat="""id              BIGINT, 
            size            DOUBLE PRECISION, 
            bed_rooms       INTEGER, 
            bath_rooms      INTEGER, 
            total_rooms     INTEGER, 
            base_rent       DOUBLE PRECISION, 
            total_rent      DOUBLE PRECISION, 
            street          VARCHAR(100), 
            district_id     INTEGER, 
            city_id         INTEGER, 
            agent_id        INTEGER, 
            created_at      TIMESTAMP, 
            last_updated_at TIMESTAMP, 
            is_active       BOOLEAN, 
            has_pictures    BOOLEAN""",
)
update_keys = dict(
    agent=[
        "first_name",
        "last_name",
        "agency_name",
        "phone_number",
        "email_address",
    ],
    flat=["id"],
)


def get_file_names(path: str) -> list:
    if os.path.isdir(path):
        return [os.path.join(path, file) for file in os.listdir(path)]
    else:
        logger.warning("%s directory does not exists", path)


def get_data(file_path):
    _list = []
    with open(file_path, "rb") as f:
        for line in f.readlines():
            data = json.loads(line)
            _list.append(data["data"])
    for record in _list:
        record.update(
            (key, "NOT AVAILABLE") for key, val in record.items() if val == ""
        )
    return _list


def create_delete_query(table_name: str, update_keys: list):
    delete_condition = " and ".join(
        [
            f"{table_name}.{field} = temp_{table_name}.{field}"
            for field in update_keys
        ]
    )
    return f"delete from {table_name} using temp_{table_name} where {delete_condition}"


def get_insert_from_temp_table_query(table_name: str, columns: list) -> str:
    if table_name == "flat":
        _query = f"insert into {table_name} ({columns}) select {columns} from temp_{table_name}"
    else:
        _query = f"insert into {table_name} ({columns}) (select {columns} from temp_{table_name} left join {table_name} using({columns}) where {table_name} is null)"
    return _query


def copy_using_temp_table(
    table_name: str, columns: str, file_path: str, update_keys: list
):
    temp_table = f"temp_{table_name}"
    temp_query = f"create temporary table {temp_table} ({schema[table_name]})"
    db.execute(temp_query)
    copy_to_table(table_name=temp_table, columns=columns, file_path=file_path)

    if table_name == "flat":
        delete_query = create_delete_query(table_name, update_keys)
        db.execute(delete_query)

    insert_query = get_insert_from_temp_table_query(table_name, columns)
    result = db.execute(insert_query)
    logger.info(
        "%s records imported into table '%s'", result.rowcount, table_name
    )
    db.execute(f"drop table {temp_table}")


def copy_to_table(table_name: str, columns: str, file_path: str):
    result = db.execute(
        text(
            f"""copy {table_name} ({columns}) from :csv
                       delimiter ',' csv header"""
        ).execution_options(autocommit=True),
        csv=file_path,
    )

    logger.info(
        "%s new records imported into table '%s'", result.rowcount, table_name
    )


def export_to_datawarehouse(df: pd.DataFrame, table_name: str, file_path: str):
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    df.to_csv(file_path, index=False)
    columns = ",".join(df.columns.tolist())
    if table_name in ["city", "district"]:
        copy_to_table(table_name, columns, file_path)
    else:
        copy_using_temp_table(
            table_name, columns, file_path, update_keys[table_name]
        )


def upsert_flats(data: list):
    city_df = pd.read_sql("select * from city", db)
    district_df = pd.read_sql("select * from district", db)
    agent_df = pd.read_sql("select * from agent", db)

    def _get_city(city: str) -> int:
        return city_df[city_df["name"] == city].id.max()

    def _get_district(district: str) -> int:
        return district_df[district_df["name"] == district].id.max()

    def _get_agent(record: dict) -> int:
        return agent_df["id"][
            (
                agent_df["first_name"]
                == record.get("contactDetails_firstname", "NOT AVAILABLE")
            )
            & (
                agent_df["last_name"]
                == record.get("contactDetails_lastname", "NOT AVAILABLE")
            )
            & (
                agent_df["agency_name"]
                == record.get("contactDetails_company", "NOT AVAILABLE")
            )
            & (
                agent_df["phone_number"]
                == record.get("contactDetails_phoneNumber", "NOT AVAILABLE")
            )
            & (
                agent_df["email_address"]
                == record.get("contactDetails_email", "NOT AVAILABLE")
            )
        ].max()

    _list = []
    for record in data:
        state = True if record["realEstate_state"] == "ACTIVE" else False
        picture_status = (
            True if record.get("realEstate_titlePicture_urls", None) else False
        )
        _list.append(
            dict(
                id=record.get("realEstate_id"),
                size=record.get("realEstate_livingSpace", None),
                total_rooms=int(record.get("realEstate_numberOfRooms", 0)),
                bed_rooms=int(record.get("realEstate_numberOfBedRooms", 0)),
                bath_rooms=int(record.get("realEstate_numberOfBathRooms", 0)),
                base_rent=record.get("realEstate_baseRent", None),
                total_rent=record.get("realEstate_calculatedTotalRent", None),
                street=record.get("realEstate_address_street", None),
                district_id=_get_district(
                    record.get("realEstate_address_quarter", None)
                ),
                city_id=_get_city(
                    record.get(
                        "realEstate_address_geoHierarchy_city_name", None
                    )
                ),
                agent_id=int(_get_agent(record)),
                created_at=record.get("realEstate_creationDate"),
                last_updated_at=record.get("realEstate_lastModificationDate"),
                is_active=state,
                has_pictures=picture_status,
            )
        )
    flat_df = pd.DataFrame(_list)
    if not flat_df.empty:
        export_filename = (
            f"{import_dir}/flat/{dt.datetime.now().isoformat()}.csv"
        )
        export_to_datawarehouse(flat_df, "flat", export_filename)


def upsert_agents(data: list):
    _list = []
    for record in data:
        _list.append(
            dict(
                first_name=record.get(
                    "contactDetails_firstname", "NOT AVAILABLE"
                ),
                last_name=record.get(
                    "contactDetails_lastname", "NOT AVAILABLE"
                ),
                agency_name=record.get(
                    "contactDetails_company", "NOT AVAILABLE"
                ),
                phone_number=record.get(
                    "contactDetails_phoneNumber", "NOT AVAILABLE"
                ),
                email_address=record.get(
                    "contactDetails_email", "NOT AVAILABLE"
                ),
            )
        )
    agents_df = pd.DataFrame(_list)
    agents_df.drop_duplicates(
        subset=update_keys["agent"], keep="first", inplace=True
    )
    if not agents_df.empty:
        export_filename = (
            f"{import_dir}/agent/{dt.datetime.now().isoformat()}.csv"
        )
        export_to_datawarehouse(agents_df, "agent", export_filename)


def upsert_districts(data: list):
    all_districts = [record["realEstate_address_quarter"] for record in data]
    unique_districts = set(all_districts)
    result = db.execute("select name from district")
    existing_districts = set([row.name for row in result])
    new_districts = list(unique_districts - existing_districts)
    if new_districts:
        export_filename = (
            f"{import_dir}/district/{dt.datetime.now().isoformat()}.csv"
        )
        districts_df = pd.DataFrame([{"name": city} for city in new_districts])
        export_to_datawarehouse(districts_df, "district", export_filename)
    else:
        logger.info("No new records to import into 'district' table")


def upsert_cities(data: list):
    all_cities = [
        record["realEstate_address_geoHierarchy_city_name"] for record in data
    ]
    unique_cities = set(all_cities)
    result = db.execute("select name from city")
    existing_cities = set([row.name for row in result])
    new_cities = list(unique_cities - existing_cities)
    if new_cities:
        export_filename = (
            f"{import_dir}/city/{dt.datetime.now().isoformat()}.csv"
        )
        cities_df = pd.DataFrame([{"name": city} for city in new_cities])
        export_to_datawarehouse(cities_df, "city", export_filename)
    else:
        logger.info("No new records to import into 'city' table")


def import_data(data: list):
    upsert_cities(data)
    upsert_districts(data)
    upsert_agents(data)
    upsert_flats(data)


def start_import():
    files = get_file_names(intial_dir)
    if files:
        for file in files:
            logger.info("Processing file - %s", file)
            data = get_data(file)
            import_data(data)
            new_path = file.replace(intial_dir, processed_dir)
            shutil.move(file, new_path)
            logger.info(
                "File moved from '%s' to new location - '%s'", file, new_path
            )
    else:
        logger.info("No files found in path %s to import", intial_dir)
        return


if __name__ == "__main__":
    start_import()
