# dwh-importer

ETL script to import flat rental data into Datawarehouse.

* [Change Log](CHANGES)
* [Installation & Configuration](INSTALL.md)

## Available ETL scripts:

 - datawarehouse_import.py

**Datawarehouse import:**
  To import falt rental data from  files on a particular path which are exported on hourly basis  into datawarehouse.


## Configuration
The imported script and alembic migration makes use of the `config.yaml` file in the project root directoy, a sample config file is provided with the name `config.yaml.sample` rename it to `config.yaml` and update the values as per your needs.
```sh
$ cp config.yaml.sample config.yaml
```

    sqlalchemy.url: "DB_CONNECTION_STRING"
    source_dir: "/PATH/TO/SOURCE_FILE_DIRECTORY"
    processed_dir: "/PATH/TO/PROCESSED_FILE_EXPORT_DIRECTORY"
    import_dir: "/PATH/TO/FILE_IMPORT_DIRECTORY"

 - sqlalchemy.url - DWH connection string, ex - `"postgresql+psycopg2://postgres:postgres@localhost:5432/prod"`
 - source_dir - File path into which the exported files are stored hourly.
 - processed_dir: File path into which the source files need to be moved once they are imported.
 - import_dir: File path into which the import files neeed to be store, files that are used for importing data into datawarehouse.

## Test
To run test simply execute the following command.
```sh
make test
```

If you don't want to follow creating virtual environment in the above mentione way, feel free to create it the way you follow and run the following to install dependent packages.
```sh
make install
```

## Execution
Once the configurations are set in the `config.yml` file the import script can be executed. Before that run the `alembic migration` to create all the facta nd dimension tables.
```sh
$ alembic upgrade head
```

Now run the importer script as follows.
```sh
rental-flat-import
```
 
**Upset techniques used:**
 - For dimension tables `city, district`
     - Only the new city/district if present is exported as csv and imported thus the `primary key` id of those dimesnion table is maintained.
 - For dimension tables `agent`
     - All available agent data from the current import file is exported as csv and imported and imported into a `temp table`. As we don't have any unique id to idendify the agent a composite key is used to identiy the agent based on the following combination `first_name, last_name, agency_name, phone_number, email_address` and only the agent data which is already not present is imported thus maintaining the primary key `id` of the agent.
     - Insert query used:
         ```sql
        INSERT INTO agent 
                        (agency_name, 
                         email_address, 
                         first_name, 
                         last_name, 
                         phone_number) 
        (SELECT agency_name, 
                email_address, 
                first_name, 
                last_name, 
                phone_number 
         FROM   temp_agent 
                LEFT JOIN agent using(agency_name, email_address, first_name, last_name, 
                                      phone_number) 
         WHERE  agent IS NULL) 
         ```
 - For fact table `flat`
     - All available flat data from the current import file is exported as csv and imported and imported into a `temp table`. As flats have a unique id `id` is being used to removed from the flat table from temp table and then the temp table is imported into flat table.
     - Upsert queires used, follwoing the order of execution:
        ```sql
        DELETE  FROM  flat  
        USING  temp_flat  
        WHERE  flat.id  =  temp_flat.id
         ```
        ```sql
            INSERT  INTO  flat  
                (agent_id,  
                base_rent,  
                bath_rooms,  
                bed_rooms,  
                city_id,  
                created_at,  
                district_id,  
                has_pictures,  
                id,  
                is_active,  
                last_updated_at,  
                size,  
                street,  
                total_rent,  
                total_rooms)  
            SELECT  agent_id,  
                base_rent,  
                bath_rooms,  
                bed_rooms,  
                city_id,  
                created_at,  
                district_id,  
                has_pictures,  
                id,  
                is_active,  
                last_updated_at,  
                size,  
                street,  
                total_rent,  
                total_rooms  
            FROM  temp_flat
        ```
